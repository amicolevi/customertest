import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { CustomerService } from '../customer.service';
import { Students } from '../interfaces/students';
import { PredictionService } from '../prediction.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  constructor(private customerService:CustomerService,
    private authService:AuthService,
    private predictionService:PredictionService ) { }

  students:Students[]; 
  students$;
  math:number;  
   psi:number; 
   pay:boolean; 
   formType:string;
   userId;
   

   deleteStudent(index){
    let id = this.students[index].id;
    this.customerService.deleteStudent(this.userId, id);
   }
  
  displayedColumns: string[] = ['email','Math', 'Psi','Predict', 'Delete'];
 

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.students$ = this.customerService.getStudents(this.userId);
          this.students$.subscribe(
            docs => {         
              this.students = [];
              var i = 0;
              for (let document of docs) {
                
                const student:Students = document.payload.doc.data();
                
                student.id = document.payload.doc.id;
                   this.students.push(student); 
              }                        
            }
          )
      })
  }


}