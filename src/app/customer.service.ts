import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

import { Students } from './interfaces/students';


@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  alert:string="You must give a years of education between 0-24";

  studentCollection:AngularFirestoreCollection;
  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');


  // getCustomers(userId): Observable<any[]>{
  //   this.customerCollection = this.db.collection(`users/${userId}/Customers`,
  //   ref => ref.orderBy('id', 'asc').limit(5));
  //   return this.customerCollection.snapshotChanges()
  // }

  getStudents(userId): Observable<any[]>{
    this.studentCollection = this.db.collection(`users/${userId}/Students`,
    ref => ref.orderBy('math', 'asc'));
    return this.studentCollection.snapshotChanges()
  }

  // nextPage(userId,startAfter): Observable<any[]>{
  //   this.customerCollection = this.db.collection(`users/${userId}/Customers`, 
  //   ref => ref.limit(5).orderBy('name', 'asc')
  //     .startAfter(startAfter))    
  //   return this.customerCollection.snapshotChanges();
  // }
  
  // prevPage(userId,startAt): Observable<any[]>{
  //   this.customerCollection = this.db.collection(`users/${userId}/Customers`, 
  //   ref => ref.limit(5).orderBy('name', 'asc')
  //     .startAt(startAt))    
  //   return this.customerCollection.snapshotChanges();
  // }



  deleteStudent(userId:string, id:string){
    this.db.doc(`users/${userId}/Students/${id}`).delete();
  }

  updateStudent(userId:string, id:string, math:number, psi:number, pay:boolean){
    this.db.doc(`users/${userId}/Students/${id}`).update(
      {
        id:id,
        math:math,
        psi:psi,
        pay:pay,
        result:null
      }
    )
  }

  updateRsult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/Students/${id}`).update(
      {
        result:result
      })
    }  

    addStudent(userId:string, math:number, psi:number, pay:boolean, answer:string,email){
      const student:Students = {math:math,psi:psi, pay:pay, answer:answer, email:email}
      this.userCollection.doc(userId).collection('Students').add(student);
  }
  


  constructor(private db:AngularFirestore) { }
}
// .pipe(map(
//   collection =>collection.map(
//     document => {
//       const data = document.payload.doc.data();
//       data.id = document.payload.doc.id;
//       return data;
//     }
//   )
// ))