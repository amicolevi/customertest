import { Response } from './interfaces/response';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Blog } from './interfaces/blog';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  blogsCollection:AngularFirestoreCollection;
  specificBlogsCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  private URL = "https://jsonplaceholder.typicode.com/posts/";
  private comments;
  private blogs;

  constructor(private http:HttpClient, private db:AngularFirestore) { }

  getBlog():Observable<Blog>{
    return this.http.get<Blog>(this.URL);
  }

  getBlogs(userId){
    this.blogsCollection = this.db.collection(`users/${userId}/Blogs`,
    ref => ref.orderBy('title', 'asc').limit(5));
    return this.blogsCollection.snapshotChanges().pipe(
      map(
        collection =>collection.map(
          document=> {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data; 
          }
        )  
     ))  
  }
  

  getComments(id):Observable<Response>{
    this.comments = `https://jsonplaceholder.typicode.com/posts/${id}/comments`;
    return this.http.get<Response>(this.comments);
  }
  
  getComment():Observable<Response>{
    return this.http.get<Response>(this.comments);
  }

  // updateBlog(userId:number, id:number, title:string, body:string){
  //   this.db.doc(`users/${userId}/Blogs/${id}`).update(
  //     {
  //       userId:userId,
  //       id:id,
  //       title:title,
  //       body:body
  //     }
  //   )
  // }

  saveBlog(userId:string, id:number, title:string, body:string){
    const blog:Blog = {id:id, title:title, body:body};
    this.userCollection.doc(userId).collection('Blogs').add(blog);
}


//   saveBlog(id):Observable<Blog>{
//     const blog = `https://jsonplaceholder.typicode.com/posts/${id}`;
//     this.userCollection.doc(userId).collection('Customers').add(customer);
//     return
// }


}
