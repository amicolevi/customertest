export interface Students {
    math: number,
    email?:string,
    psi:number,
    pay: boolean,
    id?:string,
    saved?:Boolean,
    answer?:string
  
}
