import { PredictionService } from './../prediction.service';
import { CustomerService } from './../customer.service';
import { AuthService } from './../auth.service';
import { Students } from './../interfaces/students';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  students:Students[]; 
  students$;
  math:number;  
   psi:number; 
   pay:boolean =false; 
   formType:string;
   answer;
   email;
   
   pressed:boolean=false;
 

  checkpredict:boolean = false;
  userId;
   
  saveStudent(){
    
    this.customerService.addStudent(this.userId, this.math, this.psi, this.pay, this.answer, this.email);
  }

 
  cancle(){
    this.pressed= false
  }

  predict(){
    this.pressed= true;
    this.predictionService.predictStud(this.math, this.psi, this.pay).subscribe(
      res => {
        if(res > 0.5){
          this.answer = 'yes';
        } else {
          this.answer = 'no'
        }
        
     
  })
    
  }

  constructor(public authService:AuthService, public customerService:CustomerService, public predictionService:PredictionService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          this.email = user.email;
          console.log(user.uid);
          
              })                        
            }
          
  }


