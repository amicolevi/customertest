import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  url = 'https://z4xnm1v4r9.execute-api.us-east-1.amazonaws.com/beta'; 
  
  predictStud(math:number, psi:number, pay:boolean):Observable<any>{
    let json = {
          "math": math,
          "psi": psi,
          "pay":pay

    }
    let body  = JSON.stringify(json);
    
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        return res;       
      })
    );      
  }

  
  constructor(private http: HttpClient) { }
}